using Core.Contracts;
using FluentValidation;

namespace Core.Validators
{
    public class UserUpdateDetailsRequestValidator : AbstractValidator<UpdateUserDetailsRequest>
    {
        public UserUpdateDetailsRequestValidator()
        {
            RuleFor(x => x.Description)
                .MaximumLength(250);
        }
    }
}