using FluentValidation;

namespace Core.Validators
{
    public static class ValidatorRuleBuilderExtensions
    {
        public static IRuleBuilder<T, string> NoDigits<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches("^[^0-9]+$").WithMessage("Digits not allowed");
        }
        
        public static IRuleBuilder<T, string> NoWhitespaceCharacters<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches(@"^\S+$").WithMessage("Whitespace characters not allowed");
        }

        public static IRuleBuilder<T, string> NoSpecialCharacters<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches("^[a-zA-Z0-9]*$").WithMessage("Special characters not allowed");
        }
        
        public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder, int minLength = 9)
        {
            return ruleBuilder
                .NotEmpty()
                .Length(minLength, 80)
                .Matches("[A-Z]").WithMessage("Password must contain uppercase letter")
                .Matches("[a-z]").WithMessage("Password must contain lowercase letter")
                .Matches("[0-9]").WithMessage("Password must contain digit")
                .Matches("^[a-zA-Z0-9]*$").WithMessage("Password must not contain special characters");
        }
    }
}