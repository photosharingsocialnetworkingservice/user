using Core.Contracts;
using FluentValidation;

namespace Core.Validators
{
    public class  UserRegisterRequestValidator : AbstractValidator<UserRegisterRequest>
    {
        public UserRegisterRequestValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress();
            
            RuleFor(x => x.Password)
                .Password(8);

            RuleFor(x => x.Name)
                .MinimumLength(4)
                .MaximumLength(15)
                .NotEmpty()
                .NoWhitespaceCharacters()
                .NoDigits()
                .NoSpecialCharacters();
        }
    }
}