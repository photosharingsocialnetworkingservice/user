using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entites;

namespace Core.Repositories
{
    public interface IUserRepository
    {
        Task<User?> GetUserByEmail(string email);
        Task<User?> GetUserById(int id);
        Task<User?> GetUserWithDetailsById(int id);
        Task AddUser(User user);
        Task<List<User>> GetUsersWhichEmailOrNameStartWith(string text);
        Task UpdateUserDetails(UserDetails userDetails);
        Task<List<User>> GetUsersByIds(List<int> ids);
    }
}