using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Repositories
{
    public interface IFriendshipRepository
    {
        Task AddFriendship(int inviterId, int confirmerId);
        Task RemoveFriendship(int firstUserId, int secondUserId);
        Task<bool> DoesFriendshipExist(int firstUserId, int secondUserId);
        Task<int> GetFriendsCount(int userId);
        Task<List<int>> GetAllUserFriendsIds(int userId);
    }
}