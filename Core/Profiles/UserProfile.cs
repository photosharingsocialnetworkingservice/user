using System.Collections.Generic;
using AutoMapper;
using Core.Entites;
using Core.Contracts;

namespace Core.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserRegisterRequest, User>();
            CreateMap<User, UserDataResponse>();
        }
    }
}