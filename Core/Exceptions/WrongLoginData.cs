using System;

namespace Core.Exceptions
{
    public class WrongLoginData : Exception
    {
        public WrongLoginData()
        {
        }

        public WrongLoginData(string message) : base(message)
        {
        }

        public WrongLoginData(string message, Exception inner) : base(message, inner)
        {
        }
    }
}