using System;

namespace Core.Exceptions
{
    public class FriendshipFailureException : Exception
    {
        public FriendshipFailureException()
        {
        }

        public FriendshipFailureException(string message) : base(message)
        {
        }

        public FriendshipFailureException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}