namespace Core.Contracts
{
    public class UserProfileDataResponse
    {
        public UserDataResponse User { get; set; }
        public bool? IsFriend { get; set; }
        public int FriendsCount { get; set; }
        public string Description { get; set; }
    }
}