namespace Core.Contracts
{
    public class UserDataResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}