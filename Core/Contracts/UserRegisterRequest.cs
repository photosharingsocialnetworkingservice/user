﻿namespace Core.Contracts
{
    public class UserRegisterRequest
    {
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
