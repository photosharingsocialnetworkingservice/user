using System.Collections.Generic;

namespace Core.Contracts
{
    public class EnrichedIdsWithUserDataResponse
    {
        public List<UserDataResponse> EnrichedIds { get; set; }
    }
}