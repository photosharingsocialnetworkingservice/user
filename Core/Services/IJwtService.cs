using System.Collections.Generic;

namespace Core.Services
{
    public interface IJwtService
    {
        public string GenerateToken(string email, int id);
    }
}