using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entites;
using Core.Contracts;

namespace Core.Services
{
    public interface IUserService
    {
        Task<UserDataResponse> Register(UserRegisterRequest user);
        Task<UserDataResponse> GetVerifiedUser(UserLoginRequest user);
        Task CreateFriendship(int firstUserId, int secondUserId);
        Task DeleteFriendship(int firstUserId, int secondUserId);
        Task<List<UserDataResponse>> GetUsersWhichEmailOrNameStartWith(string text);
        Task<UserProfileDataResponse> GetUserProfileDataById(int userProfileId, int? requestUserId);
        Task UpdateUserProfileDetails(int userProfileId, UpdateUserDetailsRequest userDetails);
        Task<List<int>> GetAllUserFriendsIds(int userId);
        Task<bool> DoesFriendshipExist(int id1, int id2);
        Task<List<UserDataResponse>> GetAllFriends(int userId);
    }
}