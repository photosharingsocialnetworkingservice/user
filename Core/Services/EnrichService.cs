using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Contracts;
using Core.Repositories;

namespace Core.Services
{
    public class EnrichService : IEnrichService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public EnrichService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }
        
        public async Task<EnrichedIdsWithUserDataResponse> EnrichIdsWithUserData(List<int> ids)
        {
            var users = await _userRepository.GetUsersByIds(ids);

            EnrichedIdsWithUserDataResponse response = new EnrichedIdsWithUserDataResponse
            {
                EnrichedIds = _mapper.Map<List<UserDataResponse>>(users)
            };

            return response;
        }
    }
}