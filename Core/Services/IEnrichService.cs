using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Contracts;

namespace Core.Services
{
    public interface IEnrichService
    {
        Task<EnrichedIdsWithUserDataResponse> EnrichIdsWithUserData(List<int> ids);
    }
}