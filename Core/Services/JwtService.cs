using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.IdentityModel.Tokens;

namespace Core.Services
{
    public class JwtService : IJwtService
    {
        private readonly string? _jwtSecret;

        public JwtService()
        {
            _jwtSecret = Environment.GetEnvironmentVariable("JwtSecret");
        }
        
        public string GenerateToken(string email, int id)
        {
            return new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(Encoding.ASCII.GetBytes(_jwtSecret ?? throw new NoNullAllowedException("Jwt secret can not be null")))
                .Issuer("Lamini")
                .Audience("Lamini")
                .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
                .AddClaim("email", email)
                .AddClaim("id", id)
                .Encode();
        }
    }
}