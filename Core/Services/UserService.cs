using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Entites;
using Core.Exceptions;
using Core.Contracts;
using Core.Repositories;
using MassTransit;
using BC = BCrypt.Net.BCrypt;

namespace Core.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IFriendshipRepository _friendshipRepository;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IMapper _mapper;

        public UserService(
            IUserRepository userRepository,
            IMapper mapper,
            IFriendshipRepository friendshipRepository,
            IPublishEndpoint publishEndpoint
            )
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _friendshipRepository = friendshipRepository;
            _publishEndpoint = publishEndpoint;
        }

        public async Task<UserDataResponse> Register(UserRegisterRequest user)
        {
            var existingUser = await _userRepository.GetUserByEmail(user.Email);
            if (existingUser != null) throw new UserAlreadyExistsException("User with that email already exists");

            var userEntity = _mapper.Map<User>(user);
            userEntity.PasswordHash = BC.HashPassword(user.Password, 13);
            userEntity.Details = new UserDetails();
            await _userRepository.AddUser(userEntity);
            await _publishEndpoint.Publish(new EmailContract{ ReceiverEmail = user.Email });
            
            var userResponseData = _mapper.Map<UserDataResponse>(userEntity);

            return userResponseData;
        }

        public async Task<UserDataResponse> GetVerifiedUser(UserLoginRequest user)
        {
            var userEntity = await _userRepository.GetUserByEmail(user.Email);
            
            if (userEntity == null || !BC.Verify(user.Password, userEntity.PasswordHash))
                throw new WrongLoginData("Wrong username or password");

            return _mapper.Map<UserDataResponse>(userEntity);
        }

        public async Task CreateFriendship(int inviterId, int receiverId)
        {
            if (inviterId == receiverId)
                throw new FriendshipFailureException("You can not become a friend of yourself");
                
            if (await _friendshipRepository.DoesFriendshipExist(inviterId, receiverId))
                throw new FriendshipFailureException("You already have that person on your friend list");
                
            var receiver = await _userRepository.GetUserById(receiverId);

            if (receiver == null)
                throw new FriendshipFailureException("Invited user does not exist");
            
            await _friendshipRepository.AddFriendship(inviterId, receiverId);
        }

        public async Task DeleteFriendship(int firstUserId, int secondUserId)
        {
            if (!await _friendshipRepository.DoesFriendshipExist(firstUserId, secondUserId))
                throw new FriendshipFailureException("You dont not have that person on your friend list");

            await _friendshipRepository.RemoveFriendship(firstUserId, secondUserId);
        }

        public async Task<UserProfileDataResponse> GetUserProfileDataById(int userProfileId, int? requestUserId)
        {
            var userEntity = await _userRepository
                .GetUserWithDetailsById(userProfileId);
            
            if (userEntity == null)
                throw new UserNotFoundException($"There is no user with given id: {userProfileId}");

            var description = userEntity.Details?.Description ?? "";
            var user = _mapper.Map<UserDataResponse>(userEntity);
            var friendsCount = await _friendshipRepository.GetFriendsCount(userProfileId);
            bool? isFriend = null;
            
            if (userProfileId != requestUserId && requestUserId != null)
            {
                isFriend = await _friendshipRepository.DoesFriendshipExist(userProfileId, (int)requestUserId);
            }

            UserProfileDataResponse userProfileDataResponse = new UserProfileDataResponse
            {
                User = user,
                Description = description,
                FriendsCount = friendsCount,
                IsFriend = isFriend
            };
            
            return userProfileDataResponse;
        }

        public async Task UpdateUserProfileDetails(int userProfileId, UpdateUserDetailsRequest userDetails)
        {
            var userEntity = await _userRepository
                .GetUserWithDetailsById(userProfileId);
            
            if (userEntity == null)
                throw new UserNotFoundException();

            if (userEntity.Details == null)
                userEntity.Details = new UserDetails
                {
                    Description = userDetails.Description
                };
            else
                userEntity.Details.Description = userDetails.Description;

            await _userRepository.UpdateUserDetails(userEntity.Details);
        }

        public async Task<List<int>> GetAllUserFriendsIds(int userId)
        {
            return await _friendshipRepository.GetAllUserFriendsIds(userId);
        }

        public async Task<List<UserDataResponse>> GetUsersWhichEmailOrNameStartWith(string text)
        {
            var users = await _userRepository
                .GetUsersWhichEmailOrNameStartWith(text);

            var usersResponseData = _mapper.Map<List<UserDataResponse>>(users);
            return usersResponseData;
        }

        public async Task<bool> DoesFriendshipExist(int id1, int id2)
        {
            return await _friendshipRepository.DoesFriendshipExist(id1, id2);
        }

        public async Task<List<UserDataResponse>> GetAllFriends(int userId)
        {
            var friendsIds = await _friendshipRepository.GetAllUserFriendsIds(userId);
            var users = await _userRepository.GetUsersByIds(friendsIds);

            var mappedUsers = _mapper.Map<List<UserDataResponse>>(users);
            
            return mappedUsers;
        }
    }
}