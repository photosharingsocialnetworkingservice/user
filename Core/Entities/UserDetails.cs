namespace Core.Entites
{
    public class UserDetails
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; } = "";
        public User User { get; set; }
    }
}