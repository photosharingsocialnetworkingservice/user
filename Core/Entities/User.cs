using System.Collections;
using System.Collections.Generic;

namespace Core.Entites
{
    public class User
    {
        public int Id { get; set; }
        public string PasswordHash { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public UserDetails? Details { get; set; }
        public IList<Friendship> AcceptedFriendships { get; set; }
        public IList<Friendship> RequestedFriendships { get; set; }
    }
}