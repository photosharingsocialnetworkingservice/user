namespace Core.Entites
{
    public class Friendship
    {
        public int InviterId { get; set; }
        public int ConfirmerId { get; set; }
        public User Inviter { get; set; }
        public User Confirmer { get; set; }
    }
}