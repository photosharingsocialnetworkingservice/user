﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class FriendshipEntityChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Friendships_Users_User1Id",
                table: "Friendships");

            migrationBuilder.DropForeignKey(
                name: "FK_Friendships_Users_User2Id",
                table: "Friendships");

            migrationBuilder.RenameColumn(
                name: "User2Id",
                table: "Friendships",
                newName: "ConfirmerId");

            migrationBuilder.RenameColumn(
                name: "User1Id",
                table: "Friendships",
                newName: "InviterId");

            migrationBuilder.RenameIndex(
                name: "IX_Friendships_User2Id",
                table: "Friendships",
                newName: "IX_Friendships_ConfirmerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Friendships_Users_ConfirmerId",
                table: "Friendships",
                column: "ConfirmerId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Friendships_Users_InviterId",
                table: "Friendships",
                column: "InviterId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Friendships_Users_ConfirmerId",
                table: "Friendships");

            migrationBuilder.DropForeignKey(
                name: "FK_Friendships_Users_InviterId",
                table: "Friendships");

            migrationBuilder.RenameColumn(
                name: "ConfirmerId",
                table: "Friendships",
                newName: "User2Id");

            migrationBuilder.RenameColumn(
                name: "InviterId",
                table: "Friendships",
                newName: "User1Id");

            migrationBuilder.RenameIndex(
                name: "IX_Friendships_ConfirmerId",
                table: "Friendships",
                newName: "IX_Friendships_User2Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Friendships_Users_User1Id",
                table: "Friendships",
                column: "User1Id",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Friendships_Users_User2Id",
                table: "Friendships",
                column: "User2Id",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
