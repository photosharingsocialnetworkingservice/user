using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entites;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class FriendshipRepository : IFriendshipRepository
    {
        private readonly PSSNSContext _ctx;

        public FriendshipRepository(PSSNSContext ctx)
        {
            _ctx = ctx;
        }

        public async Task AddFriendship(int inviterId, int confirmerId)
        {
            Friendship friendship = new Friendship();
            friendship.InviterId = inviterId;
            friendship.ConfirmerId = confirmerId;
            await _ctx.Friendships.AddAsync(friendship);
            await _ctx.SaveChangesAsync();
        }

        public async Task RemoveFriendship(int firstUserId, int secondUserId)
        {
            _ctx.Friendships
                .Remove(await _ctx.Friendships
                    .Where(f => (f.ConfirmerId == firstUserId || f.ConfirmerId == secondUserId)
                                && (f.InviterId == firstUserId || f.InviterId == secondUserId))
                    .FirstAsync());
            await _ctx.SaveChangesAsync();
        }

        public async Task<bool> DoesFriendshipExist(int firstUserId, int secondUserId)
        {
            return await _ctx.Friendships
                .AnyAsync(f => f.InviterId == firstUserId && f.ConfirmerId == secondUserId
                               || f.InviterId == secondUserId && f.ConfirmerId == firstUserId);
        }

        public async Task<int> GetFriendsCount(int userId)
        {
            return await _ctx.Friendships
                .Where(f => f.ConfirmerId == userId || f.InviterId == userId)
                .CountAsync();
        }

        public async Task<List<int>> GetAllUserFriendsIds(int userId)
        {
            return await _ctx.Friendships
                .Where(f => f.ConfirmerId == userId || f.InviterId == userId)
                .Select(s => s.ConfirmerId == userId ? s.InviterId : s.ConfirmerId)
                .ToListAsync();
        }
    }
}