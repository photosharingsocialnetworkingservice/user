
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entites;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly PSSNSContext _ctx;

        public UserRepository(PSSNSContext ctx)
        {
            _ctx = ctx;
        }
        
        public async Task<User?> GetUserByEmail(string email)
        {
            return await _ctx.Users
                .FirstOrDefaultAsync(u => u.Email.ToLower() == email.ToLower());
        }

        public async Task<User?> GetUserById(int id)
        {
            return await _ctx.Users
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<List<User>> GetUsersByIds(List<int> ids)
        {
            return await _ctx.Users
                .Where(u => ids.Contains(u.Id))
                .ToListAsync();
        }
        
        public async Task<User?> GetUserWithDetailsById(int id)
        {
            return await _ctx.Users
                .Include(u => u.Details)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task AddUser(User user)
        {
            await _ctx.Users.AddAsync(user);
            _ctx.SaveChanges();
        }

        public async Task<List<User>> GetUsersWhichEmailOrNameStartWith(string text)
        {
            return await _ctx.Users
                .Where(x => x.Name.StartsWith(text) || x.Email.StartsWith(text))
                .OrderBy(x => x.Name)
                .Take(10)
                .ToListAsync();
        }

        public async Task UpdateUserDetails(UserDetails userDetails)
        {
            _ctx.UserDetails
                .Update(userDetails);
                
            await _ctx.SaveChangesAsync();
        }
    }
}