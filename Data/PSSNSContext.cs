using Core.Entites;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public sealed class PSSNSContext : DbContext
    {
        public PSSNSContext(DbContextOptions<PSSNSContext> options) : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Friendship>().HasKey(f => new { f.InviterId, f.ConfirmerId });

            modelBuilder.Entity<Friendship>()
                .HasOne(f => f.Inviter)
                .WithMany(u => u.AcceptedFriendships)
                .HasForeignKey(f => f.InviterId);

            modelBuilder.Entity<Friendship>()
                .HasOne(f => f.Confirmer)
                .WithMany(u => u.RequestedFriendships)
                .HasForeignKey(f => f.ConfirmerId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Details)
                .WithOne(ud => ud.User)
                .HasForeignKey<UserDetails>(ud => ud.UserId);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Friendship> Friendships { get; set; }
        
        public DbSet<UserDetails> UserDetails { get; set; }
    }
}