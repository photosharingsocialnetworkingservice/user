using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Core.Profiles;
using Core.Repositories;
using Core.Services;
using Core.Validators;
using Data;
using Data.Repositories;
using FluentValidation.AspNetCore;
using JWT;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace Api
{
    public class Startup
    {
        private readonly IHostEnvironment _env;

        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            _env = env;
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            DotNetEnv.Env.Load();
            var rabbitHost = Environment.GetEnvironmentVariable("RABBITMQ_HOST");
            var rabbitUser = Environment.GetEnvironmentVariable("RABBITMQ_DEFAULT_USER");
            var rabbitPassword = Environment.GetEnvironmentVariable("RABBITMQ_DEFAULT_PASS");

            var rabbitConnectionString = $"amqp://{rabbitUser}:{rabbitPassword}@{rabbitHost}:5672";
            
            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(rabbitConnectionString);
                });
            });

            services.AddMassTransitHostedService();
            
            services.AddControllers()
                .AddFluentValidation(cfg => cfg.RegisterValidatorsFromAssemblyContaining<UserRegisterRequestValidator>())
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            
            services.AddAutoMapper(Assembly.GetAssembly(typeof(UserProfile)));

            string connectionString;
            
            if (_env.IsDevelopment())
            {
                connectionString = Configuration.GetConnectionString("MSSQL");
            }
            else
            {
                var DatabaseUrl = Environment.GetEnvironmentVariable("MSSQL_URL");
                var DatabaseName = Environment.GetEnvironmentVariable("MSSQL_DATABASE");
                var DatabaseUser = Environment.GetEnvironmentVariable("MSSQL_USER");
                var DatabasePassword = Environment.GetEnvironmentVariable("MSSQL_PASSWORD");
                connectionString = $"Server={DatabaseUrl}; Database={DatabaseName}; User Id={DatabaseUser}; Password={DatabasePassword}";
            }

            services.AddDbContextPool<PSSNSContext>(options => options.UseSqlServer(connectionString));
            
            services.AddSingleton<IJwtService, JwtService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFriendshipRepository, FriendshipRepository>();
            services.AddScoped<IEnrichService, EnrichService>();

            services.AddSwaggerGen();
            
            services.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Authorization");
            }));

            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = "Lamini",
                        ValidateIssuer = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.ASCII.GetBytes(Environment.GetEnvironmentVariable("JwtSecret") ??
                                                    throw new NoNullAllowedException("Jwt secret can not be null"))),
                        ValidAudience = "Lamini",
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(0)
                    };
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                DotNetEnv.Env.Load();
                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Auth Service API");
                });
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAllPolicy");
            
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
