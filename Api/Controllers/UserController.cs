﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Entites;
using Core.Exceptions;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BC = BCrypt.Net.BCrypt;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<UserDataResponse>>> GetUsers(string query)
        {
            return Ok(await _userService.GetUsersWhichEmailOrNameStartWith(query));
        }

        [AllowAnonymous]
        [HttpGet("profile/{id:int}")]
        public async Task<ActionResult<UserProfileDataResponse>> GetUserProfileData(int id)
        {
            try
            {
                var requestUserId = User.FindFirst("id")?.Value;
                int? requestUserIdParsed = null;
                
                if (requestUserId != null)
                {
                    requestUserIdParsed = int.Parse(requestUserId);
                }
                
                return Ok(await _userService.GetUserProfileDataById(id, requestUserIdParsed));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        
        [Authorize]
        [HttpPut("details")]
        public async Task<ActionResult<UserProfileDataResponse>> UpdateUserProfileDetails(UpdateUserDetailsRequest userDetails)
        {
            try
            {
                var requestUserId = int.Parse(User.FindFirstValue("id"));
                await _userService.UpdateUserProfileDetails(requestUserId, userDetails);
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
