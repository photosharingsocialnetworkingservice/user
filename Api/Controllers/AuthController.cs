﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Exceptions;
using Core.Services;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BC = BCrypt.Net.BCrypt;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IJwtService _jwtService;

        public AuthController(IUserService userService, IJwtService jwtService)
        {
            _userService = userService;
            _jwtService = jwtService;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<UserDataResponse>> Register(UserRegisterRequest user)
        {
            try
            {
                var userData = await _userService.Register(user);
                return Ok(userData);
            }
            catch (UserAlreadyExistsException e)
            {
                return BadRequest(new
                {
                    errors = new
                    {
                        Email = new [] { e.Message }
                    }
                });
            }
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserDataResponse>> Login(UserLoginRequest user)
        {
            try
            {
                var verifiedUser = await _userService.GetVerifiedUser(user);
                var jwt = _jwtService.GenerateToken(verifiedUser.Email, verifiedUser.Id);
                Response.Headers.Add("Authorization", jwt);

                return Ok(verifiedUser);
            }
            catch (WrongLoginData e)
            {
                return BadRequest(new
                {
                    errors = new
                    {
                        EmailAndPassword = new [] { e.Message }
                    }
                });
            }
        }
    }
}
