using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EnrichController : ControllerBase
    {
        private readonly IEnrichService _enrichService;

        public EnrichController(IEnrichService enrichService)
        {
            _enrichService = enrichService;
        }
        
        [HttpPost("enrichUserIdsByUserData")]
        public async Task<ActionResult<EnrichedIdsWithUserDataResponse>> EnrichUserIdsByUserData(List<int> ids)
        {
            return Ok(await _enrichService.EnrichIdsWithUserData(ids));
        }
    }
}
