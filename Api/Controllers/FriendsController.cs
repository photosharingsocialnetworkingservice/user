﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Exceptions;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BC = BCrypt.Net.BCrypt;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FriendsController : ControllerBase
    {
        private readonly IUserService _userService;

        public FriendsController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpPost("add")]
        public async Task<ActionResult> Add(FriendshipRequest friendshipRequest)
        {
            var userId = int.Parse(User.FindFirstValue("id"));
            
            try
            {
                await _userService.CreateFriendship(userId, friendshipRequest.Id);
                return NoContent();
            }
            catch (FriendshipFailureException e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [Authorize]
        [HttpDelete("remove")]
        public async Task<ActionResult> Remove(FriendshipRequest friendshipRequest)
        {
            var userId = int.Parse(User.FindFirstValue("id"));
            
            try
            {
                await _userService.DeleteFriendship(userId, friendshipRequest.Id);
                return NoContent();
            }
            catch (FriendshipFailureException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("exist")]
        public async Task<ActionResult<bool>> DoesFriendshipExist(int id1, int id2)
        {
            return await _userService.DoesFriendshipExist(id1, id2);
        }
        
        [HttpGet("user")]
        public async Task<ActionResult> GetAllUserFriendsIds(int id)
        {
            return Ok(await _userService.GetAllUserFriendsIds(id));
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<List<UserDataResponse>>> GetAllFriends()
        {
            var userId = int.Parse(User.FindFirstValue("id"));

            return Ok(await _userService.GetAllFriends(userId));
        }
    }
}
